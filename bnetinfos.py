#!/usr/bin/env python

import os
import requests

BNET_API_KEY = open(os.path.join(os.path.dirname(__file__), 'api_key')).read().strip()
BNET_LOCALE = 'de_DE'
OUR_REALMS = ('Baelgun', 'Lothar')


class BattleNet(object):
    BASE_URL = 'https://eu.api.battle.net/wow/{}'

    def __init__(self, api_key, locale):
        self.api_key = api_key
        self.locale = locale

    def _request(self, path, **params):
        params['apikey'] = self.api_key
        params['locale'] = self.locale
        return requests.get(self.BASE_URL.format(path), params=params).json()

    def realm_status(self):
        return self._request('realm/status')

    def guild_members(self, realm, guild):
        return self._request('guild/{}/{}'.format(realm, guild), fields='members')


def realm_info(bnet):
    r = bnet.realm_status()
    our_realms = []
    for realm in r['realms']:
        if realm['name'] in OUR_REALMS:
            if realm['status']:
                color = 'green'
                text = 'UP'
            else:
                color = 'red'
                text = 'DOWN'
            our_realms.append('{} <span style="color:{}">{}</span>!'.format(realm['name'], color, text))
    return '<br>'.join(our_realms)

def members(bnet):
    r = bnet.guild_members('Baelgun', 'Pirates of Honor')
    ms = []
    for member in r['members']:
        member = member['character']
        ms.append(u'<tr><td>{}</td><td>{}</td></tr>'.format(member['name'], member['realm']))
    return u'\n'.join(ms)

if __name__ == '__main__':
    bnet = BattleNet(BNET_API_KEY, BNET_LOCALE)
    #print realm_info(bnet)
    print members(bnet)
